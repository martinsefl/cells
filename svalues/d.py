#!/usr/bin/python

mesh = False

_src = ["rad", "mono"]
ener = [1,3,5,6,7,8,9,10,12,14,16,18,20,22,24,26,28,30,50,70,100]
no_prim = 100000
rns = {"mono": 100000, "99mTc": 50663,"111In": 70349,"125I":25211}
radionuklid = "r99mTc"
if (radionuklid=="99mTc"):
    print()



for en in rns:
    print(en)
    for geom in ["el1","el2","sph"]:
        macFile = "dna-"+ geom +".mac"
        with open(macFile, "w") as ff:
            ff.write("#!/bin/bash"+"\n")
    #    Geometry setting
            if (geom == "el1"):
                ff.write(
                    "/dna/det/setCytoSize 12.5 5. 2. um \n"
                    "/dna/det/setNucleusSize 8. 4. 2. um \n"
                )
            if (geom == "el2"):
                ff.write(
                    "/dna/det/setCytoSize 10. 5. 2.5 um \n"
                    "/dna/det/setNucleusSize 8. 4. 2. um \n"
                )
            if (geom == "sph"):
                ff.write(
                    "/dna/det/setCytoSize 5. 5. 5. um \n"
                    "/dna/det/setNucleusSize 4. 4. 4. um \n"
                    )
            ff.write("/dna/det/update \n")
            ff.write(
                    "/tracking/verbose 0 \n"
                    "/run/verbose 2 \n")

    #    Add a scoring mesh?
            if (mesh):
                ff.write(
                    "/score/create/boxMesh boxMesh_a \n"
                    "/score/mesh/boxSize 10. 10. 10. um \n"
                    "/score/mesh/nBin 50 50 50 \n"
                    "/score/quantity/energyDeposit eDep \n"
                    "/score/close \n"
                    )
    #
            ff.write(
                    "/dna/det/setMat G4_WATER \n"
                    "# /dna/det/setMat G4_WATER_MODIFIED \n"
                    "/gun/particle e- \n"
                    )
    #
            if (ener=="mono"):
                energie = 1
                ff.write("/gun/energy "+str(energie)+" keV \n")
            ff.write("/run/initialize \n"
                     "/process/em/auger true \n"
                     "/run/beamOn" + str(no_prim) +" \n"
                     "/score/dumpAllQuantitiesToFile boxMesh_a vox_"+geom+radionuklid+ ".txt \n"
                     )

