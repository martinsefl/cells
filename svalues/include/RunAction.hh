//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//

#ifndef RunAction_h
#define RunAction_h 1

#include "DetectorConstruction.hh"

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <iostream>

class G4Run;

class RunAction : public G4UserRunAction
{
  public:
  
    RunAction(DetectorConstruction*);
    ~RunAction();

    RunAction* GetRunAction() { return this;}

    void BeginOfRunAction(const G4Run*);
    void EndOfRunAction(const G4Run*);

    void WriteRadialDose(G4String );
    void WriteVoxelDose(G4String );
    void SumRadialDose(G4double,G4double);
    void SumVoxelDose(G4double, G4double, G4double, G4double);

  private:
    static const G4int nuOfSlices = 1000;

    // because ellipsoid size is max in x 12.5 halflength
    //
    static const G4int noVoxelsX = 125;
    static const G4int noVoxelsY = 50;
    static const G4int noVoxelsZ = 50;

    G4double voxelDose[noVoxelsX][noVoxelsY][noVoxelsZ];

    G4double limitX, limitY, limitZ;

    G4double radialDose[nuOfSlices];
    G4double maxScoreRadius;
    G4double sliceWidth,voxSliceWidthX,voxSliceWidthY,voxSliceWidthZ;
    G4String radialDoseFileName;
    G4String voxelDoseFileName;

    G4bool voxelScore;

};
#endif
