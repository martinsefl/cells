//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4VSolid.hh"
#include "G4Ellipsoid.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"

class G4Region;
class DetectorMessenger;

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:

    DetectorConstruction();

    ~DetectorConstruction();

    G4VPhysicalVolume* Construct();

    G4double GetCytoMass() {return fLogicCyto->GetMass();}
    G4double GetNucleusMass() {return fLogicNucleus->GetMass();}
        
//    G4double GetCytoRadius() {return fCytoRadius;}
//    G4double GetNucleusRadius() {return fNucleusRadius;}
    // ellipsoid case
    G4double GetCytoA() {return fCytoA;}
    G4double GetCytoB() {return fCytoB;}
    G4double GetCytoC() {return fCytoC;}

    G4double GetNucleusA() {return fNucleusA;}
    G4double GetNucleusB() {return fNucleusB;}
    G4double GetNucleusC() {return fNucleusC;}

    G4ThreeVector GetPointOnCellSurface();
    G4ThreeVector GetPointOnIrregShapeSurface();
    G4ThreeVector GetPointInCell();
    G4ThreeVector GetPointInNucleus();
    G4ThreeVector GetPointInCyto();


    G4bool MCF7() {return mcf7;}

    void ConstructIrregularShape();
    void ConstructEllipsoidCell();

    void SetMaterial (G4String);

    void UpdateGeometry();

    void dSetCytoSize(G4double, G4double, G4double);
    void dSetNucleusSize(G4double, G4double, G4double);
    
    G4LogicalVolume* GetLogicalCyto() {return fLogicCyto;};
    G4LogicalVolume* GetLogicalNucleus() {return fLogicNucleus;};

  private:

 // MS
    G4double 		   fWorldRadius;
    G4VPhysicalVolume* fPhysiWorld;
    G4LogicalVolume*   fLogicWorld;
//    change by MS
    G4Sphere*          fSolidWorld;

    G4VPhysicalVolume* fPhysiCyto;
    G4LogicalVolume*   fLogicCyto;
    //G4Ellipsoid*       fSolidCyto;

    G4VPhysicalVolume* fPhysiNucleus;
    G4LogicalVolume*   fLogicNucleus;
   // G4Ellipsoid*       fSolidNucleus;

    G4Material*        fWaterMaterial;

    G4bool 			   mcf7;

//    spherical cell
//    G4double fCytoRadius;
//    G4double fNucleusRadius;
// for ellipsoidal cell

    G4double fCytoA;
    G4double fCytoB;
    G4double fCytoC;

// ellipsoid these are semiaxes
// for mcf7 these are x,y semiaxes of the tube, fNucleusC is the height
    G4double fNucleusA;
    G4double fNucleusB;
    G4double fNucleusC;

// for mcf7

    G4double mcfCytoX;
    G4double mcfCytoY;
    G4double mcfCytoZ;

   // G4double mcfNucX;
   // G4double mcfNucY;
   // G4double mcfNucZ;



    void DefineMaterials();

    G4VPhysicalVolume* ConstructDetector();

    DetectorMessenger* fDetectorMessenger;
};
#endif
