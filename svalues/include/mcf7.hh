/*
 * mcf7.h
 *
 *  Created on: 16. 10. 2014
 *      Author: martinsefl
 */

#ifndef MCF7_H_
#define MCF7_H_

#include "cell.hh"
#include "G4SystemOfUnits.hh"

class mcf7 : public cell {
public:
	mcf7();
	virtual ~mcf7();

	virtual G4PVPlacement * ConstructNucleus();
	virtual G4PVPlacement * ConstructCyto();

	virtual G4ThreeVector GetPointOnCellSurface() { return G4ThreeVector (0.,0.,0.); }
	virtual G4ThreeVector GetPointInCell() { return G4ThreeVector (0.,0.,0.); }
	virtual G4ThreeVector GetPointInCyto() { return G4ThreeVector (0.,0.,0.); }
	virtual G4ThreeVector GetPointInNucleus() { return G4ThreeVector (0.,0.,0.); }

private:
	const double sqrt2 = 0.70710678118;
	G4double x_pos = 10.*um;
	G4double y_pos = 0.*um;
	G4double z_pos = 0.*um;
	G4double x_size = 3.*um;
	G4double y_size = 4.*um;
	G4double z_size = 3.*um;



};

#endif /* MCF7_H_ */
