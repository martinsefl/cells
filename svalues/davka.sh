#!/bin/bash

for i in 
do 
(
cat <<EOF
#!/bin/bash
#PBS -l nodes=1:ppn=1:linux:x86_64:nfs4
#PBS -l mem=256mb
#PBS -q long
#PBS -j oe
#PBS -M martin.sefl@gmail.com
cd $(pwd)
module add cmake-2.8
module add gcc-4.8.1
source /storage/brno2/home/martinsefl/G4/geant4.10.00.p02-build/geant4make.sh

echo $i
./svalues $i -src | tee log-${i}.txt 2>&1
EOF
) >run-$i.sh

chmod u+x run-$i.sh
done
