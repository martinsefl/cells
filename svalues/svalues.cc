//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

// NON MT FOR CLUSTER

#ifdef G4MULTITHREADED
  #include "G4MTRunManager.hh"
#else
  #include "G4RunManager.hh"
#endif

#include "Parser.hh"

#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "G4UIExecutive.hh"


#ifdef G4VIS_USE
  #include "G4VisExecutive.hh"
#endif

#include "ActionInitialization.hh"
#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "G4ScoringManager.hh"


// MS testing other physics
// #include "QBBC.hh"

bool IsBracket(char c);

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
unsigned TokenizeString(const std::string& i_source,
                        const std::string& i_seperators,
                        bool i_discard_empty_tokens,
                        std::vector<std::string>& o_tokens)
{
    size_t prev_pos = 0;
    size_t pos = 0;
    unsigned number_of_tokens = 0;
    o_tokens.clear();
    pos = i_source.find_first_of(i_seperators, pos);
    while (pos != std::string::npos)
    {
        std::string token = i_source.substr(prev_pos, pos - prev_pos);
        if (!i_discard_empty_tokens || token != "")
        {
            o_tokens.push_back(i_source.substr(prev_pos, pos - prev_pos));
            number_of_tokens++;
        }

        pos++;
        prev_pos = pos;
        pos = i_source.find_first_of(i_seperators, pos);
    }

    if (prev_pos < i_source.length())
    {
        o_tokens.push_back(i_source.substr(prev_pos));
        number_of_tokens++;
    }

    return number_of_tokens;
}


int main(int argc,char** argv) 
{
	int savedArgc = argc;
    Parser* parser = Parser::GetParser();

    parser->AddCommand("-gui", OptionNotCompulsory, "Select geant4 UI or just launch a geant4 terminal session", "qt");
    parser->AddCommand("-mac", WithOption, "Give a mac file to execute", "macFile.mac");
    parser->AddCommand("-rad", WithOption, "Give Radionuclide spectrum file name.", "99mTc.txt");
    parser->AddCommand("-geo", WithOption, "Do you want el1, el2 or sph1?", "el1");
    parser->AddCommand("-src", WithOption, "Set the source position: nuc, cel, ces, cyt.", "nuc");
    parser->AddCommand("-vox", WithOption, "Score in voxels?", "no");
    parser->Parse(argc,argv); // First initialize LaunchG4 and root then parse options

  //____________________________________________
    // Kill application if wrong argument in command line
    if (argc>0)
    {
        G4bool kill = false;
        for (G4int i = 1 ; i <argc ; i++)
        {
            if (strcmp(argv[i], ""))
            {
                kill=true;
                G4cerr<<"Unknown argument : "<< argv[i] <<"\n";
            }
        }
        if(kill)
        {
            G4cerr << "Program "<< argv[0] << " killed, wrong arguments." << G4endl;
            abort(); // KILL APPLICATION
        }
    }

  G4cout << "Parsed" << G4endl;

  // Choose the Random engine
  
  G4Random::setTheEngine(new CLHEP::RanecuEngine);

  // Construct the default run manager

#ifdef G4MULTITHREADED
  G4MTRunManager* runManager = new G4MTRunManager;
  runManager->SetNumberOfThreads(1);
#else
  G4RunManager* runManager = new G4RunManager;
#endif


  // Activate command-based scorer
  G4ScoringManager::GetScoringManager();

  // Seed selection from job ID (works on CENBG's cluster)
  
  G4int seed=0;
	
  const char * env = getenv("PBS_JOBID");

  G4int jobID_int = 0;

  if(env)
  {
    G4String buffer(env) ;
            
    G4String jobID_string = buffer.substr(0,  buffer.find(".borcluster1.cm.cluster"));
            
    jobID_string.erase( std::remove_if( jobID_string.begin(), jobID_string.end(), &IsBracket ), 
                        jobID_string.end());
            
    G4cout << "*** FORMATED JOB NAME="<< jobID_string << G4endl; 
            
    jobID_int = atoi(jobID_string.c_str());
  }

  seed = ((int) time(NULL)) + jobID_int;

  G4cout<<"*** INITIAL SEED="<< seed <<G4endl;
  G4Random::setTheSeed(seed);

  // Set mandatory user initialization classes
  
  // MS



  DetectorConstruction* detector = new DetectorConstruction;


  runManager->SetUserInitialization(detector);
  G4cout << "User init: " << G4endl;
  
  runManager->SetUserInitialization(new PhysicsList);

  G4cout << "User physics: " << G4endl;
//  runManager->SetUserInitialization(new QBBC);

  // User action initialization
  
  runManager->SetUserInitialization(new ActionInitialization(detector));
  

  // Initialize G4 kernel
  
  runManager->Initialize();

#ifdef G4VIS_USE
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
#endif
    
  // Get the pointer to the User Interface manager 
  
  G4UImanager* UI = G4UImanager::GetUIpointer();  

  // Adapted for multiple command line parameters
  if (savedArgc > 1)   // Define UI session for interactive mode.
    {
          G4cout << "Examining parameters" << G4endl;
          Command* commandLine = 0;

          bool gui = false;
          bool useCustomMacFile = false;
         // bool useRN  = false;

          G4UIExecutive * executive = 0;
          G4String macFile = "";
          G4String RNfile = "";
          G4String geoOpt = "";
          G4String srcOpt = "";

          if((commandLine = parser->GetCommandIfActive("-gui")))
          {
              gui = true;

              if(commandLine->fOption == "")
              {
                  executive = new G4UIExecutive(argc,argv,"qt");
                  //BuildGUIFrame();
              }
              else
              {
                  executive = new G4UIExecutive(argc,argv,commandLine->fOption);
                  //if(executive->IsGUI())
                  //    BuildGUIFrame();
              }
              UI->ApplyCommand("/control/execute vis.mac");
          } else {
          //	UImanager->ApplyCommand("/control/execute init.mac");
          }

          if((commandLine = parser->GetCommandIfActive("-mac")))
          {
              useCustomMacFile = true;
              macFile = commandLine->fOption;
          }


          if((commandLine = parser->GetCommandIfActive("-rad")))
          {
              //useRN = true;
              RNfile = commandLine->fOption;
          }

          if((commandLine = parser->GetCommandIfActive("-geo")))
          {

               geoOpt = commandLine->fOption;
          }

          if((commandLine = parser->GetCommandIfActive("-src")))
          {

               srcOpt = commandLine->fOption;
          }

          if(useCustomMacFile)
          {
              //____________________________________
              // Loop over mac files in command line
      //        if(macFile.empty() == false)

                  G4cout << "macFile = " << macFile << G4endl;

                  std::vector<std::string> tokens;
                  TokenizeString(macFile," ,{}",true,tokens);
                  std::vector<std::string>::iterator it = tokens.begin();

                  for( ; it != tokens.end() ; it++)
                  {
                      macFile = *it;
                      G4String execute = "/control/execute ";
                      execute += macFile;
                      UI->ApplyCommand(execute);
                  }

          } else {

                  if(gui)
                   {
                      executive->SessionStart();
                      delete executive;
                   } else {

					#ifdef _WIN32
						G4UIsession * session = new G4UIterminal();
					#else
						G4UIsession * session = new G4UIterminal(new G4UItcsh);
					#endif
						//UI->ApplyCommand("/control/execute dna.mac");
						session->SessionStart();
						delete session;
                   }




          }


      }


/*
  if (argc==1)   // Define UI session for interactive mode.
  { 
#ifdef _WIN32
    G4UIsession * session = new G4UIterminal();
#else
    G4UIsession * session = new G4UIterminal(new G4UItcsh);
#endif
    //UI->ApplyCommand("/control/execute dna.mac");
    session->SessionStart();
    delete session;
  }
  else           // Batch mode
  { 
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UI->ApplyCommand(command+fileName);
  }
*/

#ifdef G4VIS_USE
  delete visManager;
#endif

  delete runManager;

  return 0;
}

// for JobID extraction

bool IsBracket(char c)
{
    switch(c)
    {
    case '[':
    case ']':
        return true;
    default:
        return false;
    }
}

