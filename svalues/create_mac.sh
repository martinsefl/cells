#!/bin/bash
for i in 1 3 5 6 7 8 9 10 12 14 16 18 20 30 50 100
do 
(
cat <<EOF
#/control/execute MSvis.mac
/tracking/verbose 0
/run/verbose 2
# defining scoring mesh
#
#/dna/det/setMat G4_WATER_MODIFIED
/dna/det/setMat G4_WATER
/gun/particle e-
/gun/energy $i keV
/run/initialize
/process/em/auger true
/run/beamOn 100000
EOF
) >dna-${i}keV.mac
echo $i
done