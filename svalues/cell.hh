/*
 * cell.hh
 *
 *  Created on: 29. 10. 2014
 *      Author: martinsefl
 */

#ifndef CELL_HH_
#define CELL_HH_

class cell {
public:
	cell();

	virtual G4PVPlacement * ConstructCyto();
	virtual G4PVPlacement * ConstructNucleus();

	virtual G4ThreeVector GetPointOnCellSurface() = 0;
	virtual G4ThreeVector GetPointInCell() = 0;
	virtual G4ThreeVector GetPointInCyto() = 0;
	virtual G4ThreeVector GetPointInNucleus() = 0;

	virtual ~cell();
};

#endif /* CELL_HH_ */
