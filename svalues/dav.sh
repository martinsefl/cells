#!/bin/bash

for i in el1 el2 sph     
do 
for j in nuc cel cyt
do
for k in I125.txt  
do 
(
cat <<EOF
#!/bin/bash
#PBS -l nodes=1:ppn=1:linux:x86_64:nfs4
#PBS -l mem=256mb
#PBS -q normal
#PBS -j oe
#PBS -M martin.sefl@gmail.com
#cd /storage/brno2/home/martinsefl/cells/cells/svalues/b/
cd $(pwd)
module add cmake-2.8
module add gcc-4.8.1
source /storage/brno2/home/martinsefl/G4/geant4.10.00.p02-build/geant4make.sh

echo $i
./svalues -mac dna.mac -geo $i -src $j -rad $k | tee log${i}${j}${k} 2>&1
EOF
) >run-$i-$j-$k.sh

chmod u+x run-$i-$j-$k.sh
done
done 
done