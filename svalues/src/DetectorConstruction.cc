//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//

//#include "G4VUserDetectorConstruction.hh"

#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"
#include "G4SystemOfUnits.hh"
#include "G4UserLimits.hh"
#include "G4NistManager.hh"
#include "G4RunManager.hh"
//MS
#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4Tubs.hh"
#include "G4Para.hh"
#include "G4SubtractionSolid.hh"
#include "G4Transform3D.hh"
#include "Parser.hh"
#include "G4EllipticalTube.hh"
#include "G4ThreeVector.hh"

#include "G4Navigator.hh"
#include "G4VTouchable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

DetectorConstruction::DetectorConstruction()
:fWorldRadius(0.5*m), fPhysiWorld(NULL), fLogicWorld(NULL), fSolidWorld(NULL),
 fPhysiCyto(NULL), fLogicCyto(NULL),
 fPhysiNucleus(NULL), fLogicNucleus(NULL),
 fWaterMaterial(NULL),
 fCytoA(2.),fCytoB(2.),fCytoC(2.),fNucleusA(1.*um),fNucleusB(1.*um),fNucleusC(1.*um),
 mcfCytoX(0.), mcfCytoY(0.), mcfCytoZ(0.)
{
  // create commands for interactive definition of the detector

  mcf7 = false;
  fDetectorMessenger = new DetectorMessenger(this);
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

DetectorConstruction::~DetectorConstruction()
{
  delete fDetectorMessenger;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4VPhysicalVolume* DetectorConstruction::Construct()

{
  DefineMaterials();

  // MS - trying detector messenger
  // sphere
  dSetCytoSize(5*um, 5*um, 5*um);
  dSetNucleusSize(4*um, 4*um, 4*um);

  Parser* parser = Parser::GetParser();
  Command* commandLine = 0;
  if((commandLine = parser->GetCommandIfActive("-geo")))
  {
  	       if (commandLine->fOption=="sph") { dSetCytoSize(5.*um, 5.*um, 5.*um);
  	       	   	   	   	   	   	   	   	   	  	  	  dSetNucleusSize(4.*um, 4.*um, 4.*um);}

  	       if (commandLine->fOption=="sp1") { dSetCytoSize(8.7*um, 8.7*um, 8.7*um);
  	       	   	   	   	   	   	   	   	   	   	   dSetNucleusSize(6.1*um, 6.1*um, 6.1*um);}

  	       if (commandLine->fOption=="el1") { dSetCytoSize(12.5*um, 5.*um, 2.*um);
  	       	       	   	   	   	   	   	   	   	   	  dSetNucleusSize(8.*um, 4.*um, 2.*um);}

  	       if (commandLine->fOption=="el2") { dSetCytoSize(10.*um, 5.*um, 2.5*um);
  	       	       	   	   	   	   	   	   	   	   	  dSetNucleusSize(8.*um, 4.*um, 2.*um);}

  	       if (commandLine->fOption=="sp3") { dSetCytoSize(10.*um, 10.*um, 10.*um);
  	       	   	   	   	   	   	   	   	   	   	   dSetNucleusSize(8.*um, 8.*um, 8.*um);}

  	       if (commandLine->fOption=="el3") { dSetCytoSize(25*um, 10.*um, 4.*um);
  	       	       	   	   	   	   	   	   	   	   	  dSetNucleusSize(16.*um, 8.*um, 4.*um);}

  	       if (commandLine->fOption=="el4") { dSetCytoSize(20.*um, 10.*um, 5.*um);
  	       	   	   	   	   	   	   	   	   	   	   dSetNucleusSize(16.*um, 8.*um, 4.*um);}

  	       if (commandLine->fOption=="mcf7") { mcf7=true;}


  }

  return ConstructDetector();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void DetectorConstruction::DefineMaterials()
{ 

  // Water is defined from NIST material database
  G4NistManager * man = G4NistManager::Instance();

  G4Material * H2O = man->FindOrBuildMaterial("G4_WATER");
  
  // If one wishes to test other density value for water material, one should use instead:
  //G4Material * H2O = man->BuildMaterialWithNewDensity("G4_WATER_MODIFIED","G4_WATER",1.06*g/cm3);
  
  // Note: any string for "G4_WATER_MODIFIED" parameter is accepted
  // and "G4_WATER" parameter should not be changed
  // Both materials are created and can be selected from dna.mac

  fWaterMaterial = H2O;
  
  //G4cout << "-> Density of water material (g/cm3)=" << fWaterMaterial->GetDensity()/(g/cm/cm/cm) << G4endl;
  
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}



G4VPhysicalVolume* DetectorConstruction::ConstructDetector()
{
	  G4GeometryManager::GetInstance()->OpenGeometry();
	  G4PhysicalVolumeStore::GetInstance()->Clean();
	  G4LogicalVolumeStore::GetInstance()->Clean();
	  G4SolidStore::GetInstance()->Clean();
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

 // WORLD VOLUME
 // MS changing to spherical world
  fWorldRadius = .5*m;
  fSolidWorld = new G4Sphere("World",			     //its name
          0,fWorldRadius,0,2*M_PI,0,M_PI);  //its size

  fLogicWorld = new G4LogicalVolume(fSolidWorld,        //its solid
				    fWaterMaterial,	//its material
				    "World");		//its name
  
  fPhysiWorld = new G4PVPlacement(0,			//no rotation
  				 G4ThreeVector(),	//at (0,0,0)
                                 "World",		//its name
                                 fLogicWorld,		//its logical volume
                                 0,			//its mother  volume
                                 false,			//no boolean operation
                                 0);			//copy number


  	if (mcf7==false) {

		ConstructEllipsoidCell();

	} else {

		ConstructIrregularShape();
	}

  	// KILL CUT BELOW 12 eV

	//fLogicCyto->SetUserLimits(new G4UserLimits(DBL_MAX,DBL_MAX,DBL_MAX,12*eV));
	//fLogicNucleus->SetUserLimits(new G4UserLimits(DBL_MAX,DBL_MAX,DBL_MAX,12*eV));

	// Visualization attributes
	G4VisAttributes* worldVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,1.0)); //White
	worldVisAtt->SetVisibility(true);
	fLogicWorld->SetVisAttributes(worldVisAtt);


	G4VisAttributes* cytoVisAtt = new G4VisAttributes(G4Colour(1.0,0.0,0.0));
	cytoVisAtt->SetVisibility(true);
	fLogicCyto->SetVisAttributes(cytoVisAtt);

	G4VisAttributes* nucleusVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0));
	nucleusVisAtt->SetVisibility(true);
	fLogicNucleus->SetVisAttributes(nucleusVisAtt);

  return fPhysiWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....


void DetectorConstruction::dSetNucleusSize(G4double _nx, G4double _ny, G4double _nz)
{
	  fNucleusA = _nx; fNucleusB = _ny; fNucleusC = _nz;
}


void DetectorConstruction::dSetCytoSize(G4double _cx, G4double _cy, G4double _cz)
{
	  fCytoA = _cx;	fCytoB = _cy; fCytoC = _cz;
}

void DetectorConstruction::SetMaterial(G4String materialChoice)
{
  // Search the material by its name   
  G4Material* pttoMaterial =
    G4NistManager::Instance()->FindOrBuildMaterial(materialChoice);
  
  if (pttoMaterial) 
  {
    fWaterMaterial = pttoMaterial;
    fLogicWorld->SetMaterial(fWaterMaterial);
    G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  }
}


void DetectorConstruction::ConstructEllipsoidCell()
{

	 G4double pzBottomCut = 0.;
	 G4double pzTopCut = 0.;

	 G4Ellipsoid* fSolidCyto = new G4Ellipsoid("Cyto", fCytoA, fCytoB,  fCytoC,
	                         pzBottomCut,
	                         pzTopCut);

	 fLogicCyto = new G4LogicalVolume(fSolidCyto, fWaterMaterial, "Cyto");		//its name

	 fPhysiCyto = new G4PVPlacement(0,			//no rotation
	                                  G4ThreeVector(),	//at (0,0,0)
	                                   "Cyto",		//its name
	                                   fLogicCyto,		//its logical volume
	                                   fPhysiWorld,		//its mother  volume
	                                   false,		//no boolean operation
	                                   0);			//copy number

	  // NUCLEUS
	  // dimensions are handled by method dSetNucleusSize()
	  G4Ellipsoid* fSolidNucleus  = new G4Ellipsoid("Nucleus", fNucleusA, fNucleusB, fNucleusC,
			  	  	  	  	  	  	  pzBottomCut,  pzTopCut);

	  fLogicNucleus = new G4LogicalVolume(fSolidNucleus,  fWaterMaterial, "Nucleus");	//its name

	  fPhysiNucleus = new G4PVPlacement(0,			//no rotation
	                                   G4ThreeVector(),	//at (0,0,0)
	                                   "Nucleus",		//its name
	                                   fLogicNucleus,	//its logical volume
	                                   fPhysiCyto,		//its mother  volume
	                                   false,		//no boolean operation
	                                   0);			//copy number


}

void DetectorConstruction::ConstructIrregularShape()
{

		mcfCytoX = 6.297*um;
		//mcfCytoX = 8.33186929259*um;
		mcfCytoY = 0.70710678118*mcfCytoX;
		mcfCytoZ = 4.*2.*um/3.;

		G4double paraSizeZ = mcfCytoZ;
		G4double paraSizeY = mcfCytoY;
		G4double paraSizeX = mcfCytoX;

		G4double tan15_1 = 3.73205080757;
		G4double sin15_1 = 3.86370330516;
		G4double cylRad = paraSizeX*sin15_1;

		G4Tubs* solidCyl01 = new G4Tubs("solidCyl01", 0., cylRad, (2*paraSizeZ), 0., 2*M_PI );

		G4Para* solidCellPara = new
		G4Para("solidCellPara", paraSizeX, paraSizeY, paraSizeZ, 45*deg /*alpha*/, 0.*deg /*theta*/, 0.*deg);  // phi

		G4double shift_X = -paraSizeY;
		G4double shift_Y = -paraSizeY - paraSizeX*tan15_1;
		G4SubtractionSolid* solidCell01 = new G4SubtractionSolid("solidCell01", solidCellPara, solidCyl01, 0,
				G4ThreeVector(shift_X,shift_Y,0.));
		G4SubtractionSolid* solidCell = new G4SubtractionSolid("solidCell", solidCell01, solidCyl01, 0,
				G4ThreeVector(-shift_X,-shift_Y,0.));

		// G4LogicalVolume* tubCell = new G4LogicalVolume(solidCyl01, fWaterMaterial , "TubeCell");
		//		new G4PVPlacement(0, G4ThreeVector(shift_X, shift_Y,0.),"Tube",tubCell,fPhysiWorld, false, 0 );
		//		new G4PVPlacement(0, G4ThreeVector(-shift_X, -shift_Y,0.),"Tube",tubCell,fPhysiWorld, false, 0 );

		fLogicCyto  = new G4LogicalVolume(solidCell,  fWaterMaterial, "Cyto");		//its name

		fPhysiCyto = new G4PVPlacement(0,			//no rotation
					G4ThreeVector(0.,0.,0.),	//at (0,0,0)
					"Cyto",		//its name
					fLogicCyto,		//its logical volume
					fPhysiWorld,		//its mother  volume
					false,		//no boolean operation
					0);			//copy number

		// nucleus
		G4double nucX, nucY, nucZ;

		nucX = 5.656854*um;
		nucY = 0.5*nucX;
		nucZ = mcfCytoZ;

		fNucleusA = nucX;
		fNucleusB = nucY;
		fNucleusC = nucZ;

		G4RotationMatrix* Rot = new G4RotationMatrix;  // Rotates X and Z axes only
		Rot->rotateZ(-22.5*deg);

		G4EllipticalTube* fSolidNucleus  = new G4EllipticalTube("Nucleus",nucX, nucY, nucZ);

		fLogicNucleus = new G4LogicalVolume(fSolidNucleus,  fWaterMaterial, "Nucleus");	//its name

		fPhysiNucleus = new G4PVPlacement(     Rot,			//no rotation
			                                   G4ThreeVector(),	//at (0,0,0)
			                                   "Nucleus",		//its name
			                                   fLogicNucleus,	//its logical volume
			                                   fPhysiCyto,		//its mother  volume
			                                   false,		//no boolean operation
			                                   0);			//copy number
}

G4ThreeVector DetectorConstruction::GetPointInCell()
{
	G4ThreeVector vec;
	G4double rx=1*m; 		G4double ry=1*m; 		G4double rz=1*m;

	if (mcf7!=true) {

		G4double cA = fCytoA; 	G4double cB = fCytoB; 	G4double cC = fCytoC;

		do
		{
			rx = (2*G4UniformRand()-1)*cA;
			ry = (2*G4UniformRand()-1)*cB;
			rz = (2*G4UniformRand()-1)*cC;

		} while ((rx*rx/(cA*cA)+ry*ry/(cB*cB)+rz*rz/(cC*cC)>1));

		vec = G4ThreeVector (rx,ry,rz);

	} else {

		//
        // Calculating helping quantities to estimate
		//
		G4double tan15_1 = 3.73205080757;
		G4double sin15_1 = 3.86370330516;
		G4double cylRad  = mcfCytoX*sin15_1;

		G4double shift_X = -mcfCytoY;
		G4double shift_Y = -mcfCytoY-mcfCytoX*tan15_1;
		//
        // cutting cylinders center [shift_X, shift_Y], [-shift_X, -shift_Y]
		//
		rz = (2*G4UniformRand()-1)*mcfCytoZ; // z axis

	//	G4double sin45 = 0.70710678118;
		G4double cB = mcfCytoY; G4double cA = mcfCytoX;

		G4double S1,S2; // S bude suma (x-xS)**2 + (y-yS)**2 < R**2

		do
		{
			rx = (2*G4UniformRand()-1)*cA; // x axis
			ry = (2*G4UniformRand()-1)*cB;
			// now recline transform X
			rx = rx + ry;

			S1 = (rx - shift_X)*(rx - shift_X) + (ry - shift_Y)*(ry - shift_Y);
			S2 = (rx + shift_X)*(rx + shift_X) + (ry + shift_Y)*(ry + shift_Y);

				// is inside one of the cylinders?
		} while (( S1 < cylRad*cylRad) || (S2 < cylRad*cylRad));

		vec = G4ThreeVector (rx,ry,rz);

	}

	return vec;

}


G4ThreeVector DetectorConstruction::GetPointInCyto()
{
	G4double nA = fNucleusA;  G4double nB = fNucleusB;	G4double nC = fNucleusC;
	G4double rx=1*m; G4double ry=1*m; G4double rz=1*m;

	G4ThreeVector vec;

	if (mcf7!=true){
		G4double cA = fCytoA; 	  G4double cB = fCytoB;   	G4double cC = fCytoC;

		do
		{
			rx = (2*G4UniformRand()-1)*cA;
			ry = (2*G4UniformRand()-1)*cB;
			rz = (2*G4UniformRand()-1)*cC;

		} while ((rx*rx/(cA*cA)+ry*ry/(cB*cB)+rz*rz/(cC*cC)>1) || (rx*rx/(nA*nA)+ry*ry/(nB*nB)+rz*rz/(nC*nC)<1));

		vec = G4ThreeVector (rx,ry,rz);

	} else {
		//
		// Calculating helping quantities to estimate
		//
		G4double tan15_1 = 3.73205080757;
		G4double sin15_1 = 3.86370330516;
		G4double cylRad  = mcfCytoX*sin15_1;

		G4double shift_X = -mcfCytoY;
		G4double shift_Y = -mcfCytoY-mcfCytoX*tan15_1;
		//
		// cutting cylinders center [shift_X, shift_Y], [-shift_X, -shift_Y]
		//
		rz = (2*G4UniformRand()-1)*mcfCytoZ; // z axis

		// G4double sin45 = 0.70710678118;
		G4double cB = mcfCytoY; G4double cA = mcfCytoX;

		G4double S1,S2; // S bude suma (x-xS)**2 + (y-yS)**2 < R**2

		// we need to transform points to check if they are in the nucleus
		//
		G4ThreeVector t_vec;
		G4double tx,ty;

		do
		{
			rx = (2*G4UniformRand()-1)*cA; // x axis
			ry = (2*G4UniformRand()-1)*cB;
			// now recline transform X
			rx = rx + ry;

			S1 = (rx - shift_X)*(rx - shift_X) + (ry - shift_Y)*(ry - shift_Y);
			S2 = (rx + shift_X)*(rx + shift_X) + (ry + shift_Y)*(ry + shift_Y);

			// we need to transfer the vector for checking if it is in the nucleus
			t_vec = G4ThreeVector (rx,ry,rz).rotateZ(-22.5*deg);
			tx = t_vec.getX(); ty=t_vec.getY();

				// is inside one of the cylinders?   am I outside nucleus?                  am I inside cyto?
		} while (( S1 < cylRad*cylRad) || (S2 < cylRad*cylRad) || (tx*tx/(nA*nA)+ty*ty/(nB*nB)<1) );

		vec = G4ThreeVector (rx,ry,rz);

	}
	return vec;
}

G4ThreeVector DetectorConstruction::GetPointInNucleus()
{
	G4double rx=1*m; G4double ry=1*m; G4double rz=1*m;
	G4double pA = fNucleusA; 	G4double pB = fNucleusB; 	G4double pC = fNucleusC;
	G4ThreeVector vec ;
//	G4Navigator * navigator ;

	if (mcf7!=true){

		do
		{
			rx = (2*G4UniformRand()-1)*pA;
			ry = (2*G4UniformRand()-1)*pB;
			rz = (2*G4UniformRand()-1)*pC;

		} while (rx*rx/(pA*pA)+ry*ry/(pB*pB)+rz*rz/(pC*pC)>1) ;

		vec = G4ThreeVector (rx,ry,rz);
	} else {
		do
			{
				rx = (2*G4UniformRand()-1)*pA;
				ry = (2*G4UniformRand()-1)*pB;
				rz = (2*G4UniformRand()-1)*pC;

			} while (rx*rx/(pA*pA)+ry*ry/(pB*pB)>1) ;

		vec = G4ThreeVector (rx,ry,rz).rotateZ(22.5*deg);

		// G4cout << "Shooting from the point in mcf7" << G4endl;
	}
	return vec;
}


G4ThreeVector DetectorConstruction::GetPointOnCellSurface()
{
	G4ThreeVector vec;
	G4double a = fCytoA; G4double b = fCytoB; G4double c = fCytoC;
	if (mcf7!=true){
	// from definition of ell1 and ell2 a>b>c
		G4double x, y, z, r;
		G4double u, theta;
		G4double Rxyz2, P, rn_n;
		do {

			u = 2*G4UniformRand()-1.;
			theta = 2*M_PI*G4UniformRand();
			r = std::sqrt(1-u*u);
			x = r*cos(theta);
			y = r*sin(theta);
			z = u;

			// now x,y,z are uniformly distributed on a sphere of r = 1

			// we get point on ellipsoid surface
			x = a*x; 	y = b*y; 	z = c*z;

			// we will ensure the uniform distribution on a cell surface using
			// rejecting the point with the given probability Rxyz

			// for our ell1, ell2
			G4double Rmax = a*b; //

			Rxyz2 = b*b*c*c*x*x + a*a*c*c*y*y + a*a*b*b*z*z ;

			P = std::sqrt(Rxyz2);
			rn_n = G4UniformRand()*Rmax;
			// we will keep the point with the probability P/Rmax
			//
			} while (rn_n > P); // if the rn_n is bigger than P, we must start again
		vec = G4ThreeVector (x,y,z);
	} else {
		// if mcf7
		vec = GetPointOnIrregShapeSurface();

	}

	// for sphere - it is sufficient to shoot only from one point
	  Parser* parser = Parser::GetParser();
	  Command* commandLine = 0;
	  if((commandLine = parser->GetCommandIfActive("-geo")))
	  {
	  	       if (commandLine->fOption=="sph") { vec = G4ThreeVector(fCytoA, 0., 0.);}
	  	       if (commandLine->fOption=="sp1") { vec = G4ThreeVector(fCytoA, 0., 0.);}
	  	       if (commandLine->fOption=="sp3") { vec = G4ThreeVector(fCytoA, 0., 0.);}
	  }

	return vec;
}


G4ThreeVector DetectorConstruction::GetPointOnIrregShapeSurface()
{

	G4ThreeVector v; // vector of position
	G4double x,y,z;  // coordinates of the vector

	G4double sin15_1 = 3.86370330516;    // 1/sin(15deg)
	G4double cylRad  = mcfCytoX*sin15_1; // radius of cylinders which are subtracted from the parallelogram
	G4double tan15_1 = 3.73205080757;    // 1/tan(15deg)
	// center of circles / cylinders x and y coordinate
	G4double shift_X = -mcfCytoY;
	G4double shift_Y = -mcfCytoY-mcfCytoX*tan15_1;
	//
    // cutting cylinders center [shift_X, shift_Y], [-shift_X, -shift_Y]
	//

	// CHOOSING a side to shoot from
	//

	G4double side_choice;
	G4double top_a, curved_a, straight_a; // areas of each side

	// ********************* straight side
	// area of straight side of irreg cell is height x length of side
	straight_a = 2*mcfCytoX*2*mcfCytoZ;

	// ********************* curved side (cylinder boundary)
	// length of curved side
	// it is 30*deg of circle = 1/12 circle length
	G4double length = M_PI/6.*cylRad;
	// area of curved side = height*length
	curved_a = length*mcfCytoZ*2;


	// the top side - weird shape
	// area of top weird side = area of parallelogram - 2*circle parts
	top_a = mcfCytoX*mcfCytoX*(2*std::sqrt(2) - (M_PI/6-0.5)/((sin(M_PI/12))*(sin(M_PI/12))));
	//top_a = 98.1844*um*um;

	// testing the data
	//	G4cout << "Top surface area : " << top_a/um/um << G4endl;
	//	G4cout << "Curved surface area : " << curved_a/um/um << G4endl;
	//	G4cout << "Straight surface area : " << straight_a/um/um << G4endl;

	// we must choose from which side to shoot
	side_choice = G4UniformRand()*(top_a+curved_a+straight_a);

	// for calculations
	G4double rx,ry;
	if (side_choice < top_a) {

		// we will shoot from the top side of irregular shape
		z = mcfCytoZ; // fixed

		G4double cB = mcfCytoY; G4double cA = mcfCytoX;
		G4double S1,S2; // S1,2 = (x-xS)**2 + (y-yS)**2 < R**2

		do
		{
			rx = (2*G4UniformRand()-1)*cA; // x axis
			ry = (2*G4UniformRand()-1)*cB;
			// now recline transform X
			rx = rx + ry;
			// equation of circle - center [+-shift_X,+-shift_Y]
			S1 = (rx - shift_X)*(rx - shift_X) + (ry - shift_Y)*(ry - shift_Y);
			S2 = (rx + shift_X)*(rx + shift_X) + (ry + shift_Y)*(ry + shift_Y);
			// is inside one of the cylinders?
		} while (( S1 < cylRad*cylRad) || (S2 < cylRad*cylRad));

		x = rx; y = ry;

	} else if (side_choice < top_a+curved_a) {

		// we will shoot from the curved side of irregular shape
		G4double angle ;
		angle = (2*G4UniformRand()-1)*M_PI/12;
		// angle runs from -pi/12 to +pi/12
		x = shift_X+cylRad*sin(angle);
		y = shift_Y+cylRad*cos(angle);
		z = (2*G4UniformRand()-1)*mcfCytoZ;

	} else {

		// we will shoot from the straight side of irregular shape
		y = mcfCytoY*(2*G4UniformRand()-1);
		x = y-mcfCytoX;
		z = (2*G4UniformRand()-1)*mcfCytoZ;

	}

	// now we have point which is on the top, straight side or curved side

    v = G4ThreeVector(x,y,z);

    return v;

}



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
void DetectorConstruction::UpdateGeometry()
{
  G4RunManager::GetRunManager()->PhysicsHasBeenModified();
  G4RunManager::GetRunManager()->GeometryHasBeenModified();
  G4RunManager::GetRunManager()->DefineWorldVolume(ConstructDetector());
}
