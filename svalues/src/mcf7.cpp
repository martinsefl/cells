/*
 * mcf7.cpp
 *
 *  Created on: 16. 10. 2014
 *      Author: martinsefl
 */

#include "mcf7.hh"

#include "G4EllipticalTube.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"


mcf7::mcf7() {
	// TODO Auto-generated constructor stub

}




mcf7::~mcf7() {
	// TODO Auto-generated destructor stub
}


G4PVPlacement* mcf7::ConstructNucleus()
{

	G4LogicalVolume* logicCyto = new G4LogicalVolume(fSolidCyto,	//its solid
	                                     fWaterMaterial,	//its material
	                                     "Cyto");		//its name

    G4PVPlacement* hysiCyto = new G4PVPlacement(0,			//no rotation
	                                   G4ThreeVector(),	//at (0,0,0)
	                                   "Cyto",		//its name
	                                   fLogicCyto,		//its logical volume
	                                   fPhysiWorld,		//its mother  volume
	                                   false,		//no boolean operation
	                                   0);


	G4EllipticalTube* solidNucleus = new G4EllipticalTube("Nucleus", x_size, y_size, z_size);




	G4PVPlacement* physiNucleus = new G4PVPlacement();
	return physiNucleus;



}
