//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//

#include "Analysis.hh"

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"

#include "Randomize.hh"

#include "G4SystemOfUnits.hh"
#include "G4SteppingManager.hh"
#include "G4RunManager.hh"

#include "G4VPhysicalVolume.hh"
#include "G4Electron.hh"
#include "G4Proton.hh"
#include "G4Gamma.hh"
#include "G4Alpha.hh"
#include "G4DNAGenericIonsManager.hh"

#include "Parser.hh"

#include "G4Step.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

SteppingAction::SteppingAction(EventAction* evt)
:fEventAction(evt)
{
	  Parser* parser = Parser::GetParser();
	  Command* commandLine = 0;
	  G4String spectrumFileName;
	  if((commandLine = parser->GetCommandIfActive("-vox")))
	  {
		  if (commandLine->fOption=="yes") {voxelScore = true ;
		  } else { voxelScore = false; }
	  } else { voxelScore = false; }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void SteppingAction::UserSteppingAction(const G4Step* step)
{ 
	//	G4TouchableHandle touchable = step->GetPreStepPoint()->GetTouchableHandle();
	//	G4VPhysicalVolume* volume   = touchable->GetVolume();
	G4String volumeName = step->GetTrack()->GetVolume()->GetName();

	if (step->GetTotalEnergyDeposit()>0){
	fEventAction->AddDepositedEnergy(step->GetTotalEnergyDeposit(),volumeName);

	if (voxelScore){
		G4ThreeVector point1 = step->GetPreStepPoint()->GetPosition();
		G4ThreeVector point2 = step->GetPostStepPoint()->GetPosition();
		G4ThreeVector pointE = point1 + (point2 - point1)*G4UniformRand();

		G4double eventRadius = pointE.getR();
		G4double eventX = pointE.getX();
		G4double eventY = pointE.getY();
		G4double eventZ = pointE.getZ();

		G4double energyDeposit = step->GetTotalEnergyDeposit();

		//
		RunAction* rAction = ((RunAction*) G4RunManager::GetRunManager()->GetUserRunAction());
		rAction->SumRadialDose(eventRadius, energyDeposit);
		rAction->SumVoxelDose(eventX,eventY,eventZ,energyDeposit);
		}
	}

	//  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	//  if (step->GetTotalEnergyDeposit()>0)
	//  {analysisManager->FillH1(2, pointE.getR()/um ,step->GetTotalEnergyDeposit()/keV);}
}    
