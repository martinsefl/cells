//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//

#include "PrimaryGeneratorAction.hh"
#include "Randomize.hh"
#include "G4SystemOfUnits.hh"
#include "G4RandomDirection.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "Parser.hh"
#include "G4TransportationManager.hh"
#include "G4Navigator.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

PrimaryGeneratorAction::PrimaryGeneratorAction(DetectorConstruction *det)
 : G4VUserPrimaryGeneratorAction(),fParticleGun(0),fDetectorConstruction(det)
{
    fJobID = 0;
    fCounter = 0;
    fNEvents = 0;
    fNLines = 0;
    fCurrentIndex = 0;

    NuclSource = false;
    CytoSource = false;
    CeSuSource = false;
    CellSource = false;

    G4int n_particle = 1;
    fParticleGun  = new G4ParticleGun(n_particle);    

    ReadFromFile = false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void PrimaryGeneratorAction::Initialize()
{
	//MS: Set from where you want to shoot
    Parser* parser = Parser::GetParser();
    Command* commandLine = 0;

    if((commandLine = parser->GetCommandIfActive("-src")))
    {
        G4String srcPos = commandLine->fOption;
        if (srcPos=="nuc"){
        					G4cout << "Source is in the nucleus." << G4endl;
        					NuclSource = true;
        } else if (srcPos=="cyt"){
        					G4cout << "Source is in the cytoplasm." << G4endl;
        					CytoSource = true;
        } else if (srcPos=="ces"){
        					G4cout << "Source is on the cell surface." << G4endl;
        					CeSuSource = true;
        } else if (srcPos=="cel"){
        					G4cout << "Source is in the whole cell." << G4endl;
        					CellSource = true;
    	} else {
    						G4cout << "ZPRAVA: You did not choose the position of the source. It has been set to nucleus. " << G4endl;
    						NuclSource = true;
    	}
    }

    // READ DATA
// Get the spectrum source file name.
     G4String spectrumFileName = "";

     commandLine = 0;
     if((commandLine = parser->GetCommandIfActive("-rad")))
     {
         ReadFromFile = true;
         spectrumFileName = commandLine->fOption;
         G4cout << "Using radionuclide spectrum source file : " << spectrumFileName << G4endl;
     }

    if (ReadFromFile) {
		FILE * fp = 0;

		fp = fopen(spectrumFileName,"r");
		if (fp!=NULL) {

			G4float tmpNrj(0.);
			G4int tmpType(0);
			G4int tmpInt;
			G4int ncols = 0;

			fEnergy.clear();
			fType.clear();

			ncols = fscanf(fp,"%i",&tmpInt); // to ignore first line

			G4cout << "ZPRAVA: Read the first line " << G4endl ;

			while (1)
			{
				ncols = fscanf(fp,"%f %i",&tmpNrj, &tmpType);
				if (ncols < 0) break;
				fNLines++;
				fEnergy.push_back(tmpNrj*keV);
				fType.push_back(tmpType);
			}
			fclose(fp);
		} else {
			G4cout << "ZPRAVA: ERROR" << G4endl;
			G4cout << "ZPRAVA: File "<< spectrumFileName << " was not opened! " << G4endl;
		}
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
		// Shoot energy from file
	if (ReadFromFile){
		fCurrentIndex = 0;

		// CLUSTER: get number of events to be processed per core
		if(fNEvents == 0)
		{
		  fNEvents = G4RunManager::GetRunManager()->
					  GetCurrentRun()->GetNumberOfEventToBeProcessed();
		}
		//
		// and if fJobID starts at 1...
		//  fCurrentIndex = fCounter + (fJobID-1)*fNEvents;

		// OTHERWISE
		fCurrentIndex = fCounter;
		//

		fCounter += 1;

		//  if(fCurrentIndex > fNLines)
		//  {
		//      G4cout << "Pb : index > fNLines" << G4endl;
		//      G4cout << "index = " << fCurrentIndex << G4endl;
		//      G4cout << "fNLines = " << fNLines << G4endl;
		//      abort();
		//  }

		// if the number of lines in the energy file
		// is less than the number of events requested
		// ==> comment the "if(fCurrentIndex > fNLines)..." and uncomment the
		// following line:
		if (fCurrentIndex==fNLines) {
			G4cout << "ZPRAVA: I am finishing the whole file. Then starting again. " << G4endl;
		}
		if (fCurrentIndex > fNLines) {
		fCurrentIndex = fCurrentIndex % fEnergy.size();
		}
		// FOR IODINE FILE ONLY
		fParticleGun->SetParticleEnergy(fEnergy[fCurrentIndex]);
        if (fCurrentIndex % 100 == 0) {
	    G4cout << "- Shoot one electron with energy(eV)=" << fEnergy[fCurrentIndex] / eV << G4endl;
        }
    }

	G4ThreeVector vec = G4ThreeVector(0.,0.,0.);

    // Shoot position in nucleus, with random emission
	if (NuclSource){

			vec = fDetectorConstruction->GetPointInNucleus();
		}

    // Shoot position in cytoplasm, with random emission
	if (CytoSource){

			vec = fDetectorConstruction->GetPointInCyto();
    	}
	// Shoot pos in the whole cell randomly
	if (CellSource){

		    vec = fDetectorConstruction->GetPointInCell();
    	}

    // MS: shooting from the surface of the cell
    if (CeSuSource){
/*
		G4double a = fDetectorConstruction->GetCytoA();
		G4double b = fDetectorConstruction->GetCytoB();
		G4double c = fDetectorConstruction->GetCytoC();

		// from definition of ell1 and ell2 a>b>c
		G4double xRand, yRand, zRand;
		G4double x, y, z, r;
		G4double Rxyz2, P, rn_n;
		do {
			do {
				x = G4UniformRand();
				y = G4UniformRand();
				z = G4UniformRand();
				r = x*x+y*y+z*z;
				r = std::sqrt(r);
			} while (r==0);

			x = x/r;
			y = y/r;
			z = z/r;

			// now x,y,z are uniformly distributed on a sphere of r = 1

			// we get point on ellipsoid surface
			x = a*x;
			y = b*y;
			z = c*z;


			// we will ensure the uniform distribution on a cell surface using
			// rejecting the point with the given probability Rxyz

			// for our ell1, ell2
			G4double Rmax = a*b; //

			Rxyz2 = b*b*c*c*x*x + a*a*c*c*y*y + a*a*b*b*z*z ;

			P = std::sqrt(Rxyz2);
			rn_n = G4UniformRand()*Rmax;
			// we will keep the point with the probability P/Rmax
			//

			} while (rn_n > P); // if the rn_n is bigger than P, we must start again
		xRand = x;
		yRand = y;
		zRand = z;
*/
    		vec = fDetectorConstruction->GetPointOnCellSurface();
    	}


    //G4Navigator* theNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
    //G4VPhysicalVolume* myVolume = theNavigator->LocateGlobalPointAndSetup(vec);
    //G4cout << "Volume "  << myVolume->GetName()  << G4endl;


	fParticleGun->SetParticlePosition(vec);
	fParticleGun->SetParticleMomentumDirection(G4RandomDirection());
	//
    fParticleGun->GeneratePrimaryVertex(anEvent); 
}
