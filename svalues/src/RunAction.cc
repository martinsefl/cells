//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//
#include "DetectorConstruction.hh"
#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "Analysis.hh"
#include "G4ParticleGun.hh"
#include "G4SystemOfUnits.hh"
#include "Parser.hh"
#include <sstream>

using namespace std;
using namespace CLHEP;
//#include ""

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

RunAction::RunAction(DetectorConstruction*)
{
	// quantities for the scoring
	//

	maxScoreRadius = 10.*um;
	//limitX, limitY, limitZ
	// -12.5 12.5 in x, -5 +5 in y, z
	limitX = 12.5*um;
	limitY = 5*um;
	limitZ = 5*um;

	// nuOfSlices is set to 1000
	sliceWidth = maxScoreRadius/nuOfSlices;
	// 250, 100, 100
	voxSliceWidthX = 2*limitX/noVoxelsX;
	voxSliceWidthY = 2*limitY/noVoxelsY;
	voxSliceWidthZ = 2*limitZ/noVoxelsZ;

	G4cout << "ZPRAVA: Voxel slice widths: x,y,z " << voxSliceWidthX << " " << voxSliceWidthY << " " << voxSliceWidthZ << "\n";

	radialDoseFileName = "radialDose";
	voxelDoseFileName = "voxelDose";
	voxelScore = false;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
RunAction::~RunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void RunAction::BeginOfRunAction(const G4Run*)
{  

	  Parser* parser = Parser::GetParser();
	  Command* commandLine = 0;
	  G4String spectrumFileName;
	  if((commandLine = parser->GetCommandIfActive("-vox")))
	  {
		  if (commandLine->fOption=="yes") {voxelScore = true ;
		  } else { voxelScore = false; }
	  } else { voxelScore = false; }

	// empty scorers
	for (int i =0; i<nuOfSlices; i++ ){
			radialDose[i] = 0.;
	}

	for (G4int i=0; i<noVoxelsX; i++ ){
		for (int j = 0;  j<noVoxelsY; j++ ){
			for (int k = 0; k<noVoxelsZ; k++ ){
					voxelDose[i][j][k] = 0.;
			}
		}
	}

	// Initialize primary generator
  PrimaryGeneratorAction* primAct = ((PrimaryGeneratorAction*) G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction());
  if(primAct) primAct->Initialize();

  G4cout << "ZPRAVA: Setting the name of the output file. " << G4endl ;


  // MS: running more energies at the same time - from the spectrum file
  G4String EnergyName;
  if (primAct->AreYouReadingFromFile()) {
	  commandLine = 0;

	  if((commandLine = parser->GetCommandIfActive("-rad")))
	  {
	      EnergyName = commandLine->fOption;
	      // remove last 4 characters .txt suffix
	      EnergyName.erase(EnergyName.size()-4); // removing .txt suffix
	   //   G4cout << "Using radionuclide spectrum source file : " << spectrumFileName << G4endl;
	  }

  } else {
		G4ParticleGun* fGun = primAct->GiveParticleGun();
		G4double pEnergy = fGun->GetParticleEnergy()/eV+0.5;
		G4int pE = (G4int) pEnergy;
		stringstream Number;
		Number << pE;
		EnergyName = Number.str()+"_eV";
  }

 // MS: where the source is placed -> to the output file name
  G4String sourcePlace = "unk";
  G4int source_nu = primAct->WhereIsTheSource();
	if (source_nu == 1) { sourcePlace = "_nuc";}
	if (source_nu == 2) { sourcePlace = "_cyt";}
	if (source_nu == 3) { sourcePlace = "_cel";}
	if (source_nu == 4) { sourcePlace = "_ces";}

	EnergyName = EnergyName+sourcePlace;

if (true) {
	DetectorConstruction* detCon = ((DetectorConstruction*) G4RunManager::GetRunManager()->GetUserDetectorConstruction());
	G4int semA, semB, semC;

	semA = G4int (detCon->GetCytoA()/um);
	semB = G4int (detCon->GetCytoB()/um);
	semC = G4int (detCon->GetCytoC()/um);

	G4cout << "Cell Size is " << semA << " um " << semB << " um " << G4endl;
    G4String cellSize = "_";
    stringstream size;
    size << semA << "_" << semB << "_" << semC;
    cellSize = size.str()+"";
    G4cout << "Cell Size is " << cellSize << G4endl;

    EnergyName = EnergyName+cellSize;

	}
	// Book histograms, ntuple

	// Create analysis manager
	// The choice of analysis technology is done via selection of a namespace
	// in Analysis.hh

	G4cout << "##### Create analysis manager " << "  " << this << G4endl;
	G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

	G4cout << "Using " << analysisManager->GetType() << " analysis manager" << G4endl;

	// Create directories

	//analysisManager->SetHistoDirectoryName("histograms");
	//analysisManager->SetNtupleDirectoryName("ntuple");
	analysisManager->SetVerboseLevel(1);

	// Open an output file

	G4String fileName = "dna_"+EnergyName;

	radialDoseFileName = fileName+"rad";
	voxelDoseFileName = fileName+"vox";

	analysisManager->OpenFile(fileName);

	// Creating ntuple
	analysisManager->CreateNtuple("sv", "dnaphysics");
	analysisManager->CreateNtupleDColumn("doseCyto");
	analysisManager->CreateNtupleDColumn("doseNucleus");
	analysisManager->FinishNtuple();
}
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void RunAction::EndOfRunAction(const G4Run* aRun)
{  
  G4int nofEvents = aRun->GetNumberOfEvent();
  if ( nofEvents == 0 ) return;
  
  // print histogram statistics
  
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  
  // save histograms 
  
  analysisManager->Write();
  analysisManager->CloseFile();
  
  // save radial dose
  if (voxelScore){
	  WriteRadialDose(radialDoseFileName);

	  // save voxel dose
	  WriteVoxelDose(voxelDoseFileName);
  }

  // complete cleanup
  delete G4AnalysisManager::Instance();  
}

void RunAction::SumRadialDose(G4double rLocal, G4double edep)
{
	// check if we care
	if (rLocal<maxScoreRadius)
	{
	// compute slice number
	  G4int k = int(rLocal/sliceWidth);

	  // sum edep
	  radialDose[k] += edep;
	}
}


void RunAction::SumVoxelDose(G4double x,G4double y, G4double z, G4double edep)
{
	// check if we are in the region of the interest
	if ((x<limitX) && (y<limitY) && (z<limitZ) && (x>(-limitX)) && (y>(-limitY)) && (z>(-limitZ)) ) {
		  // compute slices number
		  G4int i = int((x+limitX)/voxSliceWidthX);
		  G4int j = int((y+limitY)/voxSliceWidthY);
		  G4int k = int((z+limitZ)/voxSliceWidthZ);

		  // sum edep
		  // G4cout << "SEGMENT::: " << i << " " << "j " << "  " << k << G4endl;
		  voxelDose[i][j][k] += edep;
	}
}

void RunAction::WriteRadialDose(G4String name)
{
	G4String fileHName = name + ".ascii";
	std::ofstream File(fileHName, std::ios::out);

	std::ios::fmtflags mode = File.flags();
	File.setf( std::ios::scientific, std::ios::floatfield );
	G4int prec = File.precision(5);

	File << "  Radial depth dose distribution \n "
	   << "\n  rLocal (nm)\t  radialDose(gray) \n" << G4endl;

	G4double r_dist;

	for (G4int k=0; k<nuOfSlices; k++) {
	 r_dist = (k+0.5)*sliceWidth;
	 File << "  "   << r_dist/nm
		  << "\t  " << radialDose[k]/joule << G4endl;
	}

	// restore default formats
	File.setf(mode,std::ios::floatfield);
	File.precision(prec);
}

void RunAction::WriteVoxelDose(G4String name)
{
    G4String fileHName = name + ".ascii";
    std::ofstream File(fileHName, std::ios::out);

    std::ios::fmtflags mode = File.flags();
    File.setf( std::ios::scientific, std::ios::floatfield );
    G4int prec = File.precision(5);

    File << " Size of voxel are in nm-- x: " << voxSliceWidthX/nm <<  " y: " << voxSliceWidthY/nm << " z: " << voxSliceWidthZ/nm << G4endl;
    File << " Number of voxels x: " << noVoxelsX << " y: " << noVoxelsY << " z: " << noVoxelsZ << G4endl;
    File << " x+-: " << limitX/nm << " y+-: " << limitY/nm << " z+-: " << limitZ/nm << " nm "<< G4endl;
    File << "  Voxel Dose distribution \n "
         << "\n  x (nm)\t  y (nm)  \t z(nm) \t  Deposit(joule) \n" << G4endl;

    G4double x_dist;
    G4double y_dist;
    G4double z_dist;

    for (G4int i=0; i< noVoxelsX; i++) {
    	for (G4int j=0; j< noVoxelsY; j++) {
    		for (G4int k=0; k< noVoxelsZ; k++) {

    			x_dist = (i+0.5)*voxSliceWidthX-limitX;
    			y_dist = (j+0.5)*voxSliceWidthY-limitY;
    			z_dist = (k+0.5)*voxSliceWidthZ-limitZ;

    			if (voxelDose[i][j][k]>0.){
    			File << x_dist/nm << " " << y_dist/nm << " " << z_dist/nm << " "
					   << " " << voxelDose[i][j][k]/joule << G4endl;
    			}
    		}
    	}
    }

	// restaure default formats
	File.setf(mode,std::ios::floatfield);
	File.precision(prec);
}
