/*
 * cell.cc
 *
 *  Created on: 29. 10. 2014
 *      Author: martinsefl
 */

#include "cell.hh"
#include "G4PVPlacement.hh"
#include "G4Solid.hh"

cell::cell() {
	// TODO Auto-generated constructor stub

}

G4PVPlacement* cell::ConstructCyto()
{
	  G4Solids* fSolidCyto = new G4Ellipsoid("Cyto",
	                         fCytoA,
	                         fCytoB,
	                         fCytoC,
	                         pzBottomCut,
	                         pzTopCut);

	  fLogicCyto = new G4LogicalVolume(fSolidCyto,	//its solid
	                                     fWaterMaterial,	//its material
	                                     "Cyto");		//its name

	  fPhysiCyto = new G4PVPlacement(0,			//no rotation
	                                   G4ThreeVector(),	//at (0,0,0)
	                                   "Cyto",		//its name
	                                   fLogicCyto,		//its logical volume
	                                   fPhysiWorld,		//its mother  volume
	                                   false,		//no boolean operation
	                                   0);			//copy number
      return
}

G4PVPlacement* cell::ConstructCyto()
	  // NUCLEUS
	  // dimensions are handled by method dSetNucleusSize()
	  fSolidNucleus  = new G4Ellipsoid("Nucleus",
	          fNucleusA,
	          fNucleusB,
	          fNucleusC,
	          pzBottomCut,
	          pzTopCut);

	  fLogicNucleus = new G4LogicalVolume(fSolidNucleus,	//its solid
	                                     fWaterMaterial,	//its material
	                                     "Nucleus");	//its name

	  fPhysiNucleus = new G4PVPlacement(0,			//no rotation
	                                   G4ThreeVector(),	//at (0,0,0)
	                                   "Nucleus",		//its name
	                                   fLogicNucleus,	//its logical volume
	                                   fPhysiCyto,		//its mother  volume
	                                   false,		//no boolean operation
	                                   0);			//copy number


	  return
}




cell::~cell() {
	// TODO Auto-generated destructor stub
}

