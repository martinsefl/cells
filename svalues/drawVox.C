{
    
    gROOT->Reset();
    gStyle->SetPalette(1);
    gROOT->SetStyle("Plain");
    Double_t scale;
    
    c1 = new TCanvas ("c1","",20,20,1000,800);

    
//
  
    TH3F *dep = new TH3F("dep","Deposits",125,-12.5,12.5,50,-5.,5.,50,-5,5.);
    

    
    Float_t _x,_y,_z, _deposit;
    Float_t vX, vY, vZ;
    Int_t nX, nY, nZ;
    Float_t limX, limY, limZ;
    Int_t ncols = 0;
    
    FILE * fp = fopen("new_b/dna_99mTc_cyt5_5_5vox.ascii", "r");
    char buffer[100];
    fgets(buffer, 100, fp);
    fgets(buffer, 100, fp);
    fgets(buffer, 100, fp);
    fgets(buffer, 100, fp);
    fgets(buffer, 100, fp);
    fgets(buffer, 100, fp);
    fgets(buffer, 100, fp);
    
    while (1)
    {
        ncols = fscanf(fp,"%f %f %f %f", &_x, &_y, &_z, &_deposit);
        
        if (ncols <= 0) break;
        cout << _deposit << endl;
        //cout << _x << endl;
        dep->Fill(_x*0.001,_y*0.001,_z*0.001,_deposit);

    }
    
    dep->Draw("BOX");
    
    
    fclose(fp);
    

}
