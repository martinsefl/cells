#!/bin/bash
for i in 125I 111In 99mTc #1 3 # 5 6 7 8 9 10 12 14 16 18 20 22 24 26 28 30 100
do
if [$i=125I] 
then
primaries=2519800
elif [$i=111In] 
primaries=703490
elif [$i=99mTc]
primaries=506630
fi  
(
cat <<EOF
/tracking/verbose 0
/run/verbose 2
#/dna/det/setMat G4_WATER_MODIFIED
/dna/det/setMat G4_WATER
/gun/particle e-
/run/initialize
/process/em/auger true
/run/beamOn 
EOF
) >dna${i}.mac
echo $i
done