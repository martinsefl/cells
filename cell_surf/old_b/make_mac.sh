#!/bin/bash
for i in 0.5 1 5 10
do 
(
cat <<EOF
#/control/execute vis.mac
/tracking/verbose 0
/run/verbose 2
#/dna/det/setMat G4_WATER_MODIFIED
/dna/det/setMat G4_WATER
/gun/particle e-
/gun/energy $i keV
/run/initialize
/process/em/auger true
/run/beamOn 1
EOF
) >dna-${i}keV.mac
echo $i
done