{
    gROOT->Reset();
    gStyle->SetPalette(1);
    gROOT->SetStyle("Plain");
    Double_t scale;
    Int_t nlines = 0;
    c1 = new TCanvas ("c1","",0,0,1000,550);
    //c1->SetPad("pad","",0.1, 1.0, 0.1, 0.9);
    TH1F* expe = new TH1F("EXP", "Spectrum of I", 1000, 0, 560000);
    FILE *ex = fopen("eI131e50000.dat", "r");
    Float_t  le, numb;
    Int_t entries = 0;

    ncols = fscanf(ex, "%i", &entries);
    cout << "Number of entries " << entries << endl;
    while (1)
    {
        ncols = fscanf(ex,"%f %f",&le, &numb);
        if (ncols <= 0) break;
        //cout << le << endl;
        expe->Fill(le);
        nlines++;
    }

                      
    fclose(ex);
    gPad->SetLogy();
    expe->Draw("");
    c1->Print("spectrum.png");
                      
}