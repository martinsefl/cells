//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4NistManager.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"

class G4Region;
class DetectorMessenger;

class DetectorConstruction : public G4VUserDetectorConstruction
{
  public:

    DetectorConstruction();

    ~DetectorConstruction();

    G4VPhysicalVolume* Construct();
  
    G4double GetCytoMass() {return fLogicCyto->GetMass();}
    G4double GetNucleusMass() {return fLogicNucleus->GetMass();}
        
    G4double GetCytoRadius() {return fCytoRadius;}
    G4double GetNucleusRadius() {return fNucleusRadius;}

    // ellipsoid case
    /*
    G4double GetCytoA() {return fCytoA;}
    G4double GetCytoB() {return fCytoB;}
    G4double GetCytoC() {return fCytoC;}

    G4double GetNucleusA() {return fNucleusA;}
    G4double GetNucleusB() {return fNucleusB;}
    G4double GetNucleusC() {return fNucleusC;}
     */
    void SetMaterial (G4String);            
    void UpdateGeometry();
    
    G4LogicalVolume* GetLogicalCyto() {return fLogicCyto;};
    G4LogicalVolume* GetLogicalNucleus() {return fLogicNucleus;};
                         
  private:
 // MS
 /*   G4double           fWorldSizeX;
    G4double           fWorldSizeY;
    G4double           fWorldSizeZ;
  */
 // MS
    G4double 		   fWorldRadius;


    G4VPhysicalVolume* fPhysiWorld;
    G4LogicalVolume*   fLogicWorld;
//    G4Box*          fSolidWorld;
    // change by MS, originally
    // G4Box*             fSolidWorld;
    G4Sphere*          fSolidWorld;


    G4VPhysicalVolume* fPhysiCyto;
    G4LogicalVolume*   fLogicCyto;
    G4Sphere*          fSolidCyto;

    G4VPhysicalVolume* fPhysiNucleus;
    G4LogicalVolume*   fLogicNucleus;
    G4Sphere*          fSolidNucleus;

    G4Material*        fWaterMaterial;

    G4double fCytoRadius;
    G4double fNucleusRadius;
    
  /*  // for ellipsoidal cell
    G4bool elliptic;

    G4Ellipsoid* fEllSolidCyto;


    G4Ellipsoid* fEllSolidNucleus;

    G4double fCytoA;
    G4double fCytoB;
    G4double fCytoC;

    G4double fNucleusA;
    G4double fNucleusB;
    G4double fNucleusC;

*/
    void DefineMaterials();

    G4VPhysicalVolume* ConstructDetector();     

    // MS
    G4VPhysicalVolume* ConstructEllipsoidCell();

    //
    DetectorMessenger* fDetectorMessenger;

};
#endif
