//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//

#include "EventAction.hh"
#include "DetectorConstruction.hh"
#include "Analysis.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

EventAction::EventAction(DetectorConstruction* det)
:fDetectorConstruction(det)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

EventAction::~EventAction()
{}


void EventAction::BeginOfEventAction(const G4Event*)
{
  fTotAccumulatedEnergyCyto = 0;
  fTotAccumulatedEnergyNucleus = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void EventAction::EndOfEventAction(const G4Event* event /*evt*/)
{
  
   // get analysis manager
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  // fill ntuple
  
  analysisManager->FillNtupleDColumn(0, 
   (fTotAccumulatedEnergyCyto/joule)/
    (fDetectorConstruction->GetCytoMass()/kg-fDetectorConstruction->GetNucleusMass()/kg) );
  
  analysisManager->FillNtupleDColumn(1, 
   (fTotAccumulatedEnergyNucleus/joule)/(fDetectorConstruction->GetNucleusMass()/kg));
  
  analysisManager->AddNtupleRow();  

  //G4cout << (fTotAccumulatedEnergyCyto/joule)
  // /(fDetectorConstruction->GetCytoMass()/kg-fDetectorConstruction->GetNucleusMass()/kg)  << G4endl;
  //G4cout << (fTotAccumulatedEnergyNucleus/joule)/(fDetectorConstruction->GetNucleusMass()/kg)  << G4endl;

  // MS print how it goes
  G4int eventID = event->GetEventID();
    if ( eventID % 100 == 0) {
      G4cout << ">>> Event: " << eventID  << G4endl;
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void EventAction::AddDepositedEnergy(G4double val, const G4String& volumeName)
{
    if (volumeName=="Cyto")    fTotAccumulatedEnergyCyto    = fTotAccumulatedEnergyCyto+val;
    if (volumeName=="Nucleus") fTotAccumulatedEnergyNucleus = fTotAccumulatedEnergyNucleus+val;
}
