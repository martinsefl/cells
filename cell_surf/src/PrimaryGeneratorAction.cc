//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// This example is provided by the Geant4-DNA collaboration
// Any report or published results obtained using the Geant4-DNA software 
// shall cite the following Geant4-DNA collaboration publication:
// Med. Phys. 37 (2010) 4692-4708
// The Geant4-DNA web site is available at http://geant4-dna.org
//

#include "PrimaryGeneratorAction.hh"
#include "Randomize.hh"
#include "G4SystemOfUnits.hh"
#include "G4RandomDirection.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

PrimaryGeneratorAction::PrimaryGeneratorAction(DetectorConstruction *det)
 : G4VUserPrimaryGeneratorAction(),fParticleGun(0),fDetectorConstruction(det)
{
    fJobID = 0;
    fCounter = 0;
    fNEvents = 0;
    fNLines = 0;
    fCurrentIndex = 0;

    G4int n_particle = 1;
    fParticleGun  = new G4ParticleGun(n_particle);    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void PrimaryGeneratorAction::Initialize()
{
    // READ DATA

    FILE * fp = 0;
    fp = fopen("eI131e50000.dat","r");
    
    G4float tmpNrj(0.);
    G4int tmpType(0);
    G4int tmpInt;
    G4int ncols = 0;

    fEnergy.clear();
    fType.clear();
    
    ncols = fscanf(fp,"%i",&tmpInt); // to ignore first line
    while (1)
    {
        ncols = fscanf(fp,"%f %i",&tmpNrj, &tmpType);
        if (ncols < 0) break;
        fNLines++;
        fEnergy.push_back(tmpNrj*eV);
        fType.push_back(tmpType);
    }
    fclose(fp);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    // Shoot energy from file
   /*
    fCurrentIndex = 0;

    // CLUSTER: get number of events to be processed per core
    if(fNEvents == 0)
    {
      fNEvents = G4RunManager::GetRunManager()->
                  GetCurrentRun()->GetNumberOfEventToBeProcessed();
    }
    // 
    // and if fJobID starts at 1...
    //  fCurrentIndex = fCounter + (fJobID-1)*fNEvents;
    
    // OTHERWISE
    fCurrentIndex = fCounter;

    //
    
    fCounter += 1;

    if(fCurrentIndex > fNLines)
    {
        G4cout << "Pb : index > fNLines" << G4endl;
        G4cout << "index = " << fCurrentIndex << G4endl;
        G4cout << "fNLines = " << fNLines << G4endl;
        abort();
    }*/

    // if the number of lines in the energy file
    // is less than the number of events requested
    // ==> comment the "if(fCurrentIndex > fNLines)..." and uncomment the 
    // following line:
    // fCurrentIndex = fCurrentIndex % fEnergy.size();
    
    // FOR IODINE FILE ONLY
    
    //  fParticleGun->SetParticleEnergy(fEnergy[fCurrentIndex]);
    // MS
    //    fParticleGun->SetParticleEnergy(10*eV);

    //
    
   // G4cout << "- Shoot one electron with energy(eV)=" << fEnergy[fCurrentIndex] / eV << G4endl;

    // Shoot position in nucleus, with random emission
/* MS
    G4double radius = fDetectorConstruction->GetNucleusRadius();

    G4double rx=1*m;
    G4double ry=1*m;
    G4double rz=1*m;

    do
    {
        rx = (2*G4UniformRand()-1)*radius;
        ry = (2*G4UniformRand()-1)*radius;
        rz = (2*G4UniformRand()-1)*radius;

    } while (sqrt(rx*rx+ry*ry+rz*rz)>radius) ;

    fParticleGun->SetParticlePosition(G4ThreeVector(rx,ry,rz));

    fParticleGun->SetParticleMomentumDirection(G4RandomDirection());
*/
    // MS: shooting from the surface of the cell
    G4double cytoRadius = fDetectorConstruction->GetCytoRadius();


    fParticleGun->SetParticlePosition(G4ThreeVector(0.,0.,cytoRadius));
    fParticleGun->SetParticleMomentumDirection(G4RandomDirection());

    //

    fParticleGun->GeneratePrimaryVertex(anEvent); 

}
