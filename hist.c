{
     gROOT->Reset();   
     gStyle->SetPalette(1);   
     gROOT->SetStyle("Plain");   
     Double_t scale;   
    
     c1 = new TCanvas ("c1","",0,0,1000,550);   
     //c1->SetPad("pad","",0.1, 1.0, 0.1, 0.9);  
     TH1F* expe = new TH1F("EXP", "", 1000, 0, 100000);
     FILE *ex = fopen("eI131e50000.dat”, "r"); 
     float_t  le, numb;
         
     while (1)
         { 
             
              ncols = fscanf(ex,"%f %f",&le, &numb);
              if (ncols <= 0) break; 
              expe->Fill(le);
          }
    
     expe->Draw();

}